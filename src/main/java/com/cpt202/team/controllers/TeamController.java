package com.cpt202.team.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cpt202.team.models.Team;
import com.cpt202.team.services.TeamService;


import java.util.List;
import java.util.ArrayList;


@RestController
@RequestMapping("/team")
public class TeamController {
    @Autowired
    private TeamService TeamService;
    
    @GetMapping("/list")
    public List<Team> getList(){
        return TeamService.getTeamList();
    }
    
    @PostMapping("/add")
    public Team addTeam(@RequestBody Team team){
        return TeamService.newTeam(team);
        

    }
}
